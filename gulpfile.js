var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var injectReload = require('gulp-inject-reload');
var gutil = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var symlink = require('gulp-symlink');
var data = require('./src/data.json')
var spritesmith = require('gulp.spritesmith');

var dev = !(process.env.PRODUCTION == "true");

gulp.task('html', function () {
  gulp.src('./src/*.jade')
    .pipe(jade({
      pretty: true,
      locals: {
        galleryHot: data["gallery-hot"],
        clearence: data["clearence"]
      }
    }))
    .on('error', gutil.log)
    .pipe(dev ? injectReload() : gutil.noop())
    .pipe(gulp.dest('./build/'))
    .pipe(livereload());
});

gulp.task('css', function () {
  gulp.src('./src/stylesheets/**/*.sass')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: ['./node_modules']
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build/stylesheets/'))
    .pipe(livereload());
});

browserifyBuild = function (watch) {
  opts = {
    entries: "./src/javascripts/main.js",
    debug: false,
    cache: {}, // required by watchify
    packageCache: {}, // required by watchify
    fullPaths: watch // required to be true only for watchify
  }

  var bundler = browserify(opts);
  if (watch) bundler = watchify(bundler);

  rebundle = function () {
    bundler.bundle()
      .on("error", gutil.log.bind(gutil, "Browserify error"))
      .pipe(source("main.js")) // name of the output file
      .pipe(buffer())
      .pipe(sourcemaps.init({
        loadMaps: true
      }))
      .pipe(uglify())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./build/javascripts/'))
      .pipe(livereload());
  }

  bundler.on("update", function () {
    rebundle();
    gutil.log("Rebundle..");
  });

  bundler.on("log", gutil.log); // output build logs

  rebundle();
}

var buildJS = function () {
  return browserifyBuild(false);
}
var watchJS = function () {
  return browserifyBuild(true);
}

gulp.task('javascript', buildJS);

gulp.task('video', function () {
  gulp.src('./src/video/summer-woman-collection.*')
    .pipe(symlink(function (file) {
      return './build/video/' + file.relative;
    }, { force: true }));
});

gulp.task('images', function () {
  gulp.src('./src/pic/**/*.*')
    .pipe(gulp.dest('./build/pic/'))

  gulp.src('./src/img/**/*.*')
    .pipe(gulp.dest('./build/img/'))
});

gulp.task('sprite', function () {
  var spriteData = gulp.src('./src/img/sprite/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: '_sprite.sass',
    imgPath: '/img/sprite.png'
  }));
  var spriteMobileData = gulp.src('./src/img/sprite-mobile/*.png').pipe(spritesmith({
    retinaSrcFilter: './src/img/sprite-mobile/*@2x.png',
    imgName: 'sprite-mobile.png',
    retinaImgName: 'sprite-mobile@2x.png',
    cssName: '_sprite-mobile.sass',
    imgPath: '/img/sprite-mobile.png',
    retinaImgPath: '/img/sprite-mobile@2x.png'
  }));
  spriteData.img.pipe(gulp.dest('./build/img/'));
  spriteData.css.pipe(gulp.dest('./src/stylesheets/'));

  spriteMobileData.img.pipe(gulp.dest('./build/img/'));
  spriteMobileData.css.pipe(gulp.dest('./src/stylesheets/'));
});

gulp.task('build', ['html', 'sprite', 'css', 'javascript', 'video', 'images']);

gulp.task('default', ['build'], function () {
  livereload.listen();
  gulp.watch('./src/**/*.jade', ['html']);
  gulp.watch('./src/stylesheets/**/*.sass', ['css']);
  watchJS();
});