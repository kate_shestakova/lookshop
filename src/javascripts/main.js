var $ = require("jquery");
require("./mq.genie")

$(document).ready(function () {
  // console.log($("h2"));
});

//Переключатель валют
var currencyList = $(".main-header-top__currency-menu");
var currentCurrency = $(".main-header-top__currency--current");

currentCurrency.on("click", function () {
  currencyList.fadeToggle(150);
});

currencyList.on("click", function (event) {
  event.preventDefault();
  target = $(event.target);
  currentCurrency.text(target.text());
  currencyList.fadeToggle(100);
});

$(".main-header-top__search-form").on("submit", function (event) {
  if (!$(this).hasClass("open")) {
    event.preventDefault();
    $(this).addClass("open");
    $(this).find("input").focus();
  } else {
    // validate
    if ($(this).find("input").val() == "") {
      event.preventDefault();
      $(this).find("input").focus();
    }
  }
});

//Главное меню (добавить закрытие при нажатии в любом месте страницы)
var menu = $(".main-navigation__list li");
menu.each(function () {
  var link = $(this).find(".main-navigation__link"),
    subMenu = $(this).find(".main-navigation__sublist");
  link.on("click", function (event) {
    event.preventDefault();
    if (!subMenu.hasClass("open"))
      $(".main-navigation__sublist.open").removeClass("open").fadeOut(150);
    subMenu.slideToggle(150);
    subMenu.toggleClass("open");
  });
});

$(".main-navigation h3").on("click", function (event) {
  event.preventDefault();
  $(".main-navigation__list").slideToggle(250);
})

//Корзина
$(".cart h3").on("click", function (event) {
  event.preventDefault();
  $(".cart-items").fadeToggle(150);
});

//Видео
var video = $("video");

function showVideoFallback() {
  $(".main-photo-wrapper").css("transition", "opacity 0.5s ease-in").addClass("main-photo-wrapper--show");
};

video.on("canplay", function() {
  video.css("visibility", "visible");
});

$("video source").on("error", function() {
  if(video.prop("networkState") == HTMLMediaElement.NETWORK_NO_SOURCE) {
    $(".main-photo-wrapper").addClass("main-photo-wrapper--show");
  }
});

video.on("ended", showVideoFallback);


//Burger menu в футере
var serviceLinks = $(".service");
$(".footer__burger-menu").on("click", function (event) {
  event.preventDefault();
  var scroll_to = serviceLinks.offset().top + serviceLinks.height() + serviceLinks.closest(".main-footer--bottom").css("padding-bottom");
  serviceLinks.slideToggle(250);
  $("html, body").animate({
    scrollTop: scroll_to
  }, 250);
});